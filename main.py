import webapp2
import re
import jinja2
import os

from string import letters
from google.appengine.ext import db

"""
  Jinja Environment templates
"""
templateDir = os.path.join(os.path.dirname(__file__), 'templates')
jinjaLoader = jinja2.FileSystemLoader(templateDir)
env = jinja2.Environment(loader = jinjaLoader, autoescape = True)

"""
  Global method to render Jinja HTML template as string
"""
def renderString(template, **params):
  t = env.get_template(template)
  return t.render(params)

"""
  Base Request Handler class to be derived by future handlers
"""
class BaseHandler(webapp2.RequestHandler):

  #Render template as string
  def renderTemp(self, template, **params):
    return renderString(template, **params)
  
  def render(self, template, **kwargs):
    self.write(self.renderTemp(template, **kwargs))
  
  def write(self, *args, **kwargs):
    self.response.out.write(*args,**kwargs)

import urllib2
import re
from bs4 import BeautifulSoup

class xLinksHandler(BaseHandler):
  
  def getHTML(self,pageURL):
    try:
      return urllib2.urlopen(pageURL).read()
    except:
      return None

  def stripper(self,pageURL):
    soup = BeautifulSoup(pageURL)
    p_ids = []
    for link in soup.find_all('a'):
      url = unicode(link.get('href'))
      m = re.findall(r"(?<=povid=).*", url)
      if m:
        p_ids = p_ids + m
    return "\n".join(p_ids)


  def renderPage(self, success="", error="", url="http://www.walmart.com/cp/1100706", linkIDs=""):
    return self.render("xlinks.html", success=success, error=error,
      url=url, linkIDs=linkIDs)

  def get(self):
    self.renderPage()

  def post(self):
    url = self.request.get("pageURL")
    page = self.getHTML(url)
    error = ""
    success = ""
    linkIDs= ""
    if page:
      links = self.stripper(page)
      if links:
        success = "IDs found on page"
        linkIDs = links
        self.renderPage(success,error,url,linkIDs)
      else:
        error = "No IDs found on page"
        self.renderPage(success,error,url,linkIDs)
    else:
      error = "Invalid URL. Make sure you included http://"
      self.renderPage(success,error,url,linkIDs)

class allProductsHandler(BaseHandler):

  def getHTML(self,pageURL):
    try:
      return urllib2.urlopen(pageURL).read()
    except:
      return None

  def renderPage(self, success="", error="", url="", itemIDs=""):
    return self.render("allTheLinks.html", success=success, error=error,
      url=url, itemIDs = itemIDs)

  def parseID(self, inputUrl):
    pageVar = 0
    product_ids = {}

    while True:

      pageUrl = inputUrl + "tab_value=all&ss=false&ic=60_" + str(pageVar)

      page_html = self.getHTML(pageUrl)

      if page_html:

        soup = BeautifulSoup(page_html)

        if pageVar > 1000: 
          break

        for tag in soup.find_all("input"):
          name = tag.get("name")
          if name and name == "product_id":
            product_ids[tag['value']] = tag['value']

        pageVar += 60
      
      else:
        break

    return product_ids.keys()

  def get(self):
    self.renderPage()

  def post(self):
    inputUrl = self.request.get("pageURL")

    success = ""
    itemIDs = ""
    error = ""
    url = inputUrl

    try:
      inputUrl = re.findall(r'.*\?',inputUrl)[0]
    except:
      error = "Invalid URL. Make sure it's the full url with an ? symbol somewhere"
      self.renderPage(success,error,url,itemIDs)
      return 

    itemIDs = self.parseID(inputUrl)

    if itemIDs:
      success = "Id's found!"
      itemIDs = "\n".join(itemIDs)
      self.renderPage(success,error,url,itemIDs)
    else:
      error = "No IDs found"
      self.renderPage(success,error,url,itemIDs)

app = webapp2.WSGIApplication([
    (r'/xlinks', xLinksHandler),
    (r'/allProducts', allProductsHandler),
  ], debug = True)